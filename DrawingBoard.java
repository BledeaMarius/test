package partial;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard extends JFrame {

    ArrayList<implementeazaProdus> produses = new ArrayList<>();
    private int x=20,y=50;

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(800, 800);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);

    }

    public void adauga(implementeazaProdus p) {

        produses.add(p);
        this.repaint();
    }
  public void paint(Graphics g)
  {int i=1;
      for (implementeazaProdus p:produses) {
          p.draw(g, x+i*80, y, p.getCantitate(),p.getNume());
            i++;
      }
  }

}