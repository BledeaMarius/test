package partial;

import java.awt.*;

class implementeazaProdus implements Produs {
    private int pret, cantitate;
    private String nume;

    public implementeazaProdus(int pret, int cantitate, String nume) {
        this.pret = pret;
        this.cantitate = cantitate;
        this.nume = nume;


    }

    @Override
    public int getPret() {
        return pret;
    }

    public void setPret(int pret) {
        this.pret = pret;
    }

    @Override
    public int getCantitate() {
        return cantitate;
    }
    public void incrementareCantitate()
    {
    cantitate++;
    }
    public void decrementeazaCantitate()
    {
        cantitate--;

    }
    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    @Override
    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    @Override
    public String toString() {
        return
                "pret=" + pret +
                ", cantitate=" + cantitate +
                ", nume='" + nume + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof implementeazaProdus)) return false;
        implementeazaProdus that = (implementeazaProdus) obj;
        return nume.equals(that.getNume());
    }
    public void draw (Graphics g,int x,int y,int height,String nume)
    {    nume=nume+" "+ getCantitate();
            g.drawRect(x,y,20,height);
            g.drawString(nume,x,y);


    }

}


