package partial;

import org.hamcrest.internal.ArrayIterator;

import java.util.ArrayList;
import java.util.Iterator;

public class Stoc implements IStocManager {
   ArrayList<implementeazaProdus> produses=new ArrayList<>();
    @Override
    public void adaugaProdus(implementeazaProdus p) {
     produses.add(p);
    }
    public void afisareProduse()
    {
        for(implementeazaProdus p:produses)
            System.out.println(p.toString());
    }

    @Override
    public void stergeProdus(String numeProdus) {
        Iterator<implementeazaProdus> i= produses.iterator();
        while(i.hasNext())
        {
            implementeazaProdus p= i.next();

            if(p.getNume().equals(numeProdus))
            {    i.remove();

            }}
    }

    @Override
    public void afiseazaValoareStoc() {
    //private Afiseaza valoare totala a stocului: sum(prod.pret*prod.cantitate)
        int sum=0;
        for(implementeazaProdus p:produses)
            if(p!=null)
                sum+=p.getPret()*p.getCantitate();
        System.out.println("Valoarea totala a stocului: "+sum);
    }

    @Override
    public void afiseazaProdusCantitateMinima() {
            int minim=9999;
            String nume_prod=null;
            for(implementeazaProdus p:produses)
                if(p.getCantitate()<=minim)
                {minim=p.getCantitate();
                nume_prod=p.getNume();}

        System.out.println("produsul cu cantiate minima : "+nume_prod+" si are "+minim+" unitati");

    }

    @Override
    public void incrementeazaStocProdus(String numeProdus) {
        Iterator<implementeazaProdus> i=produses.iterator();
        while(i.hasNext())
        {
            implementeazaProdus p=i.next();
            if(p.getNume().equals(numeProdus))
                p.incrementareCantitate();
        }
    }

    @Override
    public void decrementeazaStocProdus(String numeProdus) {
        Iterator<implementeazaProdus> i=produses.iterator();
        while(i.hasNext())
        {
            implementeazaProdus p=i.next();
            if(p.getNume().equals(numeProdus))
                p.decrementeazaCantitate();
        }

    }

    public static void main(String[] args) {
        Stoc stoc=new Stoc();
        implementeazaProdus p1=new implementeazaProdus(10,40,"rosii");
        implementeazaProdus p2=new implementeazaProdus(12,50,"castraveti");
        implementeazaProdus p3=new implementeazaProdus(23,100,"gogosari");
        DrawingBoard b=new DrawingBoard();
        stoc.adaugaProdus(p1);
        stoc.adaugaProdus(p2);
        stoc.adaugaProdus(p3);
        stoc.afiseazaValoareStoc();
        b.adauga(p1);
        b.adauga(p2);
        b.adauga(p3);
        stoc.stergeProdus("castraveti");

        stoc.afisareProduse();
        stoc.afiseazaProdusCantitateMinima();
        stoc.decrementeazaStocProdus("rosii");
        stoc.incrementeazaStocProdus("gogosari");

        System.out.println("-----");
        stoc.afisareProduse();

    }
}
