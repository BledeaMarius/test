package partial;



public interface Produs  {

    int getPret();
    int getCantitate();
    String getNume();

}
